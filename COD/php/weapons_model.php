<?php

require __DIR__ . "./sql/sql_conexion.php";
require __DIR__ . "./sql/sql_function.php";

class Weapons
{
    public $name;
    private $urlImage;
    private $kills;
    public $type;


    public function create_weapon($name, $urlImage, $kills, $type)
    {
        $this->name = $name;
        $this->urlImage = $urlImage;
        $this->kills = $kills;
        $this->type = $type;
    }




    public function save_weapon($conexion)
    {
        try {
            $character_data = (object) [
                'weapons_name' => $this->name,
                'weapons_url_image' => $this->urlImage,
                'weapons_kills' => $this->kills,
                'weapon_type' => $this->type,
                'weapons_id' => "NULL",

            ];

            $data_arr = array();


            foreach ($character_data as $key => $value) {
                $object = (object) ['key' => $key, 'value' => $value];
                array_push($data_arr, $object);
            }

            $sql_instruction_insert = sql_generator_insert('weapons', $data_arr);
            $preparation_instruction_insert =  $conexion->prepare($sql_instruction_insert);
            $preparation_instruction_insert->execute();
        } catch (\Throwable $error) {
            error_log($error);
            mostrar($error);
        }
    }
}


function weapon_exist($conexion, $ArrWeapons)
{
    try {
        $sql_instruction_search_all = 'SELECT * FROM `weapons`';
        $preparation_instruction_search_all =  $conexion->prepare($sql_instruction_search_all);
        $preparation_instruction_search_all->execute();
        $weapons_database = $preparation_instruction_search_all->fetchAll();

        for ($i = 0; $i < sizeof($ArrWeapons); $i++) {
            $weapon_found =  filter($ArrWeapons[$i]->name, $weapons_database, "weapons_name");
            if (!$weapon_found) {
                $ArrWeapons[$i]->save_weapon($conexion);
            }
        }

        $sql_instruction_search_all = 'SELECT * FROM `weapons`';
        $preparation_instruction_search_all =  $conexion->prepare($sql_instruction_search_all);
        $preparation_instruction_search_all->execute();
        $weapons_database = $preparation_instruction_search_all->fetchAll();
    } catch (\Throwable $error) {
        error_log($error);
        mostrar($error);
    }
    return $weapons_database;
}


function update_arr_weapons($ArrWeapons, $WeaponsNames, $WeaponsImgs, $WeaponsType)
{
    for ($i = 0; $i < sizeof($WeaponsNames); $i++) {
        $weapon = new Weapons();
        $weapon->create_weapon(
            $WeaponsNames[$i],
            $WeaponsImgs[$i],
            0,
            $WeaponsType
        );
        array_push($ArrWeapons, $weapon);
    }
    return $ArrWeapons;
}


$ArrWeapons = array();

// Submachine Gun

$SubmachineGunsNames = array('Kuda', 'VMP', 'Weevil', 'Vesper', 'Pharo', 'Razorback');
$SubmachineGunsImages = array(
    '../../images/Kuda_Model_BO3.webp',
    '../../images/VMP_Gunsmith_model_BO3.webp',
    '../../images/Weevil_Gunsmith_model_BO3.webp',
    '../../images/Vesper_Gunsmith_model_BO3.webp',
    '../../images/Pharo_Gunsmith_model_BO3.webp',
    '../../images/Razorback_Gunsmith_model_BO3.webp',
);


// Assault Rifle

$AssaultRiflesNames = array('KN-44', 'XR-2', 'HVK-30', 'ICR-1', 'Man-O-War', 'Sheiva', 'M8A7');
$AssaultRiflesImages = array(
    '../../images/KN-44_Gunsmith_model_BO3.webp',
    '../../images/XR-2_Gunsmith_model_BO3.webp',
    '../../images/HVK-30_Gunsmith_model_BO3.webp',
    '../../images/ICR-1_Gunsmith_model_BO3.webp',
    '../../images/Man-O-War_Gunsmith_model_BO3.webp',
    '../../images/4de9324723c8259e06d13b0197b57237ffebe9e3_00.jpg',
    '../../images/M8A7_Gunsmith_model_BO3.webp',
);


// Shotgun
$ShotgunsNames = array('KRM-262', '205 Brecci', 'Haymaker 12', 'Argus');
$ShotgunsImages = array(
    '../../images/KRM-262_Gunsmith_model_BO3.webp',
    '../../images/205_Brecci_Gunsmith_model_BO3.webp',
    '../../images/Haymaker_12_Gunsmith_model_BO3.webp',
    '../../images/Argus_Gunsmith_model_BO3.webp',
);


// Light Machine Gun
$LightMachineGunsNames = array('BRM', 'Dingo', 'Gorgon', '48 Dredge');
$LightMachineGunsImages = array(
    '../../images/BRM_Gunsmith_model_BO3.webp',
    '../../images/Dingo_Gunsmith_model_BO3.webp',
    '../../images/Gorgon_Gunsmith_model_BO3.webp',
    '../../images/48_Dredge_Gunsmith_model_BO3.webp',
);


// Sniper Rifle

$SniperRiflesNames = array('Drakon', 'Locus', 'P-06', 'SVG-100');
$SniperRiflesImages = array(
    '../../images/Drakon_Gunsmith_model_BO3.webp',
    '../../images/Locus_Gunsmith_model_BO3.webp',
    '../../images/P-06_Gunsmith_model_BO3.webp',
    '../../images/SVG-100_Gunsmith_model_BO3.webp',
);

//Pistol
$PistolsNames = array('MR6', 'RK5', 'L-CAR 9');
$PistolsImages = array(
    '../../images/MR6_Gunsmith_model_BO3.webp',
    '../../images/RK5.webp',
    '../../images/L-CAR_9_Gunsmith_model_BO3.webp',
);






$ArrWeapons = update_arr_weapons($ArrWeapons, $SubmachineGunsNames, $SubmachineGunsImages, 'Submachine Gun');
$ArrWeapons = update_arr_weapons($ArrWeapons, $AssaultRiflesNames, $AssaultRiflesImages, 'Assault Rifle');
$ArrWeapons = update_arr_weapons($ArrWeapons, $ShotgunsNames, $ShotgunsImages, 'Shotgun');
$ArrWeapons = update_arr_weapons($ArrWeapons, $LightMachineGunsNames, $LightMachineGunsImages, 'Light Machine Gun');
$ArrWeapons = update_arr_weapons($ArrWeapons, $SniperRiflesNames, $SniperRiflesImages, 'Sniper Rifle');
$ArrWeapons = update_arr_weapons($ArrWeapons, $PistolsNames, $PistolsImages, 'Pistol');

weapon_exist($conexion, $ArrWeapons);