<?php

if ($_POST) {

    $User_Data = (object) [
        'name' => trim($_POST['name']),
        'surname' => trim($_POST['surname']),
        'age' => $_POST['age'],
        'type' => $_POST['type'],
        'description' => $_POST['description'],
    ];

    $User_DB = array();

    $User_DB[$User_Data->name] = $User_Data;

    print_r($User_DB);
    echo '<br><br>';
    echo 'USER DATA';
    echo '<br><br>';
    echo 'Name: ' . $User_DB[$User_Data->name]->name;
    echo '<br><br>';
    echo 'Surname: ' . $User_DB[$User_Data->name]->surname;
    echo '<br><br>';
    echo 'Age: ' . $User_DB[$User_Data->name]->age;
    echo '<br><br>';
    echo 'Type: ' . $User_DB[$User_Data->name]->type;
    echo '<br><br>';
    echo 'Description: ' . $User_DB[$User_Data->name]->description;
}
?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>User record</title>
</head>

<body>

    <form method="post">
        <br>
        Name:
        <input type="text" name="name" required>
        <br>
        <br>
        Surname:
        <input type="text" name="surname" required>
        <br>
        <br>
        Age:
        <input type="number" name="age" required>
        <br>
        <br>
        Type:
        <br>
        <input type="radio" name="type" value="spirit" required>Spirit
        <br>
        <input type="radio" name="type" value="goddess" required>Goddess
        <br>
        <input type="radio" name="type" value="human" required>Human
        <br>
        <br>
        Description:
        <br>
        <textarea name="description" cols="30" rows="10"></textarea>
        <br>
        <br>
        <input type="submit" value="Send">
    </form>

</body>

</html>