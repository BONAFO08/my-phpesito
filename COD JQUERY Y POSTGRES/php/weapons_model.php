<?php

require  "reusables/data managers/charge_data_managers.pack.php";
require "reusables/postgres managers/charge_postgres_managers.pack.php";

class Weapons
{
    public $name;
    private $urlImage;
    private $kills;
    public $type;


    public function create_weapon($name, $urlImage, $kills, $type)
    {
        $this->name = $name;
        $this->urlImage = $urlImage;
        $this->kills = $kills;
        $this->type = $type;
    }




    public function save_weapon($conexion)
    {
        try {
            $character_data = (object) [
                'name' => "'$this->name'",
                'urlImage' => "'$this->urlImage'",
                'kills' => $this->kills,
                'type' => "'$this->type'",
            ];

            $data_arr = object_to_arr($character_data);

            $postgres_instruction_insert = postgres_generator_insert('weapons', $data_arr);
            return postgres_send($conexion, $postgres_instruction_insert);
        } catch (\Throwable $error) {
            error_log($error);
            mostrar($error);
        }
    }
}


function weapon_exist($conexion, $ArrWeapons)
{
    try {

        $postgres_instruction_search_all = postgres_generator_search_all('weapons');
        $weapons_database = postgres_send($conexion, $postgres_instruction_search_all);

        for ($i = 0; $i < sizeof($ArrWeapons); $i++) {
            $weapon_found =  filter_arr($ArrWeapons[$i]->name, '==', $weapons_database->msj, 'name');
            if (!$weapon_found->boolean) {
                $ArrWeapons[$i]->save_weapon($conexion);
            }
        }
    } catch (\Throwable $error) {
        error_log($error);
        mostrar($error);
    }
    return $weapons_database;
}


function update_arr_weapons($ArrWeapons, $WeaponsNames, $WeaponsImgs, $WeaponsType)
{
    for ($i = 0; $i < sizeof($WeaponsNames); $i++) {
        $weapon = new Weapons();
        $weapon->create_weapon(
            $WeaponsNames[$i],
            $WeaponsImgs[$i],
            0,
            $WeaponsType
        );
        array_push($ArrWeapons, $weapon);
    }
    return $ArrWeapons;
}


$ArrWeapons = array();

// Submachine Gun

$SubmachineGunsNames = array('Kuda', 'VMP', 'Weevil', 'Vesper', 'Pharo', 'Razorback');
$SubmachineGunsImages = array(
    '../../images/Kuda_Model_BO3.webp',
    '../../images/VMP_Gunsmith_model_BO3.webp',
    '../../images/Weevil_Gunsmith_model_BO3.webp',
    '../../images/Vesper_Gunsmith_model_BO3.webp',
    '../../images/Pharo_Gunsmith_model_BO3.webp',
    '../../images/Razorback_Gunsmith_model_BO3.webp',
);


// Assault Rifle

$AssaultRiflesNames = array('KN-44', 'XR-2', 'HVK-30', 'ICR-1', 'Man-O-War', 'Sheiva', 'M8A7');
$AssaultRiflesImages = array(
    '../../images/KN-44_Gunsmith_model_BO3.webp',
    '../../images/XR-2_Gunsmith_model_BO3.webp',
    '../../images/HVK-30_Gunsmith_model_BO3.webp',
    '../../images/ICR-1_Gunsmith_model_BO3.webp',
    '../../images/Man-O-War_Gunsmith_model_BO3.webp',
    '../../images/4de9324723c8259e06d13b0197b57237ffebe9e3_00.jpg',
    '../../images/M8A7_Gunsmith_model_BO3.webp',
);


// Shotgun
$ShotgunsNames = array('KRM-262', '205 Brecci', 'Haymaker 12', 'Argus');
$ShotgunsImages = array(
    '../../images/KRM-262_Gunsmith_model_BO3.webp',
    '../../images/205_Brecci_Gunsmith_model_BO3.webp',
    '../../images/Haymaker_12_Gunsmith_model_BO3.webp',
    '../../images/Argus_Gunsmith_model_BO3.webp',
);


// Light Machine Gun
$LightMachineGunsNames = array('BRM', 'Dingo', 'Gorgon', '48 Dredge');
$LightMachineGunsImages = array(
    '../../images/BRM_Gunsmith_model_BO3.webp',
    '../../images/Dingo_Gunsmith_model_BO3.webp',
    '../../images/Gorgon_Gunsmith_model_BO3.webp',
    '../../images/48_Dredge_Gunsmith_model_BO3.webp',
);


// Sniper Rifle

$SniperRiflesNames = array('Drakon', 'Locus', 'P-06', 'SVG-100');
$SniperRiflesImages = array(
    '../../images/Drakon_Gunsmith_model_BO3.webp',
    '../../images/Locus_Gunsmith_model_BO3.webp',
    '../../images/P-06_Gunsmith_model_BO3.webp',
    '../../images/SVG-100_Gunsmith_model_BO3.webp',
);

//Pistol
$PistolsNames = array('MR6', 'RK5', 'L-CAR 9');
$PistolsImages = array(
    '../../images/MR6_Gunsmith_model_BO3.webp',
    '../../images/RK5.webp',
    '../../images/L-CAR_9_Gunsmith_model_BO3.webp',
);



$postgres_conection = postgres_connect('Cod', 5432, 'localhost', 'postgres', '123');


function create_weapon_table($postgres_conection)
{
    $table_data = (object) [
        'id' => postgres_colum_generator('serial', 'NOT NULL PRIMARY KEY'),
        'name' => postgres_colum_generator('varchar(30)', 'NOT NULL UNIQUE'),
        'urlImage' => postgres_colum_generator('varchar(80)', 'NOT NULL'),
        'kills' => postgres_colum_generator('int8', 'NOT NULL'),
        'type' => postgres_colum_generator('varchar(30)', 'NOT NULL'),
    ];

    $table_data = object_to_arr($table_data);

    $postgres_instrution_create_table =   postgres_generator_create_table('weapons', $table_data);

    mostrar($postgres_instrution_create_table);

    return postgres_send($postgres_conection, $postgres_instrution_create_table);
};

create_weapon_table($postgres_conection);


$ArrWeapons = update_arr_weapons($ArrWeapons, $SubmachineGunsNames, $SubmachineGunsImages, 'Submachine Gun');
$ArrWeapons = update_arr_weapons($ArrWeapons, $AssaultRiflesNames, $AssaultRiflesImages, 'Assault Rifle');
$ArrWeapons = update_arr_weapons($ArrWeapons, $ShotgunsNames, $ShotgunsImages, 'Shotgun');
$ArrWeapons = update_arr_weapons($ArrWeapons, $LightMachineGunsNames, $LightMachineGunsImages, 'Light Machine Gun');
$ArrWeapons = update_arr_weapons($ArrWeapons, $SniperRiflesNames, $SniperRiflesImages, 'Sniper Rifle');
$ArrWeapons = update_arr_weapons($ArrWeapons, $PistolsNames, $PistolsImages, 'Pistol');

weapon_exist($postgres_conection, $ArrWeapons);



