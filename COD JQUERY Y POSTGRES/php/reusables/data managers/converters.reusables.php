<?php


function object_to_arr($object)
{
    $arr = [];

    foreach ($object as $key => $value) {
        $object_aux = (object) ['key' => $key, 'value' => $value];
        array_push($arr, $object_aux);
    }
    return $arr;
};
