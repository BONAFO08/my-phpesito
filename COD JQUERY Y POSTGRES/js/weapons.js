const weapons = getCookie('weapons');

let jsonData = fromStringToJSON(weapons);

const weaponLvUP = 5000;

jsonData = JSON.parse(jsonData);

jsonData.sort(compare);


const cssMain = document.createElement('link');
$(cssMain)
    .attr({
        rel: "stylesheet",
        href: "../../css/main.css"
    })
    .appendTo('head');



const cssWeapons = document.createElement('link');
$(cssWeapons)
    .attr({
        rel: "stylesheet",
        href: "../../css/weapons.css"
    })
    .appendTo('head');



const principalDiv = document.createElement('div');
$(principalDiv).appendTo('body');



const returnButton = document.createElement('a');
$(returnButton)
    .attr({
        href: '../../home.html',
        class: 'text return'
    })
    .text('RETURN')
    .appendTo(principalDiv);



const showPerks = (weaponContainer,weaponName, numbersOfPerks) => {
    const perkDiv = document.createElement('div');
    $(perkDiv)
        .attr({
            class: 'div-perk'
        })
        .appendTo(weaponContainer);

    for (let i = 1; i <= numbersOfPerks; i++) {



        $(document.createElement('img'))
            .attr({
                class: 'perk',
                src: `../../images_perks/${weaponName}/${i}.webp`,
                title: '',
            })
            .appendTo(perkDiv)
            .tooltip({
                track: true,
                content: $(document.createElement('img'))
                    .attr({
                        class: 'perk_mini',
                        src: `../../images_perks/${weaponName}/${i}.webp`
                    })
            })
            ;


        if ((i % 7) == 0) {
            $(document.createElement('br'))
                .appendTo(perkDiv);
        }
    }
}





//FORMULA
//(LV * 500)  + (LV * 2500)




jsonData.map(weapon => {

    console.log(weapon);

    const weaponContainer = document.createElement('div');
    $(weaponContainer)
        .attr(
            { class: 'container', }
        )
        .appendTo(principalDiv);;



    const weaponImage = document.createElement('img');
    $(weaponImage)
        .attr({
            src: weapon.urlimage,
            class: 'weapon_image',
        })
        .appendTo(weaponContainer);



    const weaponTitle = document.createElement('h3');
    $(weaponTitle)
        .attr({
            class: 'text',
        })
        .css({
            margin: '-65vh 0vh 5vh 80vh',
            'font-size': "8vh",
        })
        .text(weapon.name)
        .appendTo(weaponContainer);



    // const weaponKills = document.createElement('h3');
    // $(weaponKills)
    //     .attr({
    //         class: 'text',
    //     })
    //     .css({
    //         margin: '10vh 0vh 0vh 80vh',
    //         'font-size': "6vh"
    //     })
    //     .text("EXP: " + weapon.kills)
    //     .appendTo(weaponContainer);


    showPerks(weaponContainer,weapon.name,weapon.perks);

    // showPerks(weaponContainer,weapon.name, 38);


    // const weaponPerks = document.createElement('h3');
    // $(weaponPerks)
    //     .attr({
    //         class: 'text',
    //     })
    //     .css({
    //         margin: '10vh 0vh 0vh 85vh',
    //         'font-size': "6vh"
    //     })
    //     .text(`UNLOCKED PERKS: ${Math.floor(weapon.kills / weaponLvUP)}`)
    //     .appendTo(weaponContainer);


    const weaponFormAdd = document.createElement('form');
    $(weaponFormAdd)
        .attr({
            method: 'POST',
            class: 'form'
        })
        .appendTo(weaponContainer);


    const weaponAddKillsInput = document.createElement('input');
    $(weaponAddKillsInput)
        .attr({
            placeholder: "ADD EXP TO " + weapon.name,
            name: 'kills',
            type: 'number',
            class: 'input'
        })
        .css({
            margin: '0vh 0vh 0vh 120vh',
        })
        .appendTo(weaponFormAdd);



    const weaponAddNameWeapon = document.createElement('input');
    $(weaponAddNameWeapon)
        .attr({
            name: 'name',
            value: weapon.name,
            type: 'hidden'
        })
        .appendTo(weaponFormAdd);



    const weaponAddKillsButton = document.createElement('input');
    $(weaponAddKillsButton)
        .attr({
            value: "+",
            type: 'submit',
            class: 'sub-button'
        })
        .css({
            margin: '5vh 0vh 0vh 5vh',
            padding: "0vh 3vh 0vh 3vh"
        })
        .click((e) => {
            e.preventDefault();
            const kills = parseInt(weaponAddKillsInput.value) + parseInt(weapon.kills);
            weaponAddKillsInput.value = kills;
            weaponFormAdd.submit();
        })
        .appendTo(weaponFormAdd);


    const weaponFormDec = document.createElement('form');
    $(weaponFormDec)
        .attr({
            method: 'POST',
            class: 'form'
        })
        .appendTo(weaponContainer);


    const weaponDecKillsInput = document.createElement('input');
    $(weaponDecKillsInput)
        .attr({
            placeholder: "REMOVE EXP TO " + weapon.name,
            name: 'kills',
            type: 'number',
            class: 'input'
        })
        .css({
            margin: '1vh 0vh 0vh 120vh',
        })
        .appendTo(weaponFormDec);



    const weaponDecNameWeapon = document.createElement('input');
    $(weaponDecNameWeapon)
        .attr({
            name: 'name',
            value: weapon.name,
            type: 'hidden'
        })
        .appendTo(weaponFormDec);



    const weaponDecKillsButton = document.createElement('input');
    $(weaponDecKillsButton)
        .attr({
            value: "-",
            type: 'submit',
            class: 'sub-button'
        })
        .css({
            margin: '5vh 0vh 0vh 6vh',
            padding: "0vh 3.5vh 0vh 3.5vh"
        })
        .click((e) => {
            e.preventDefault();
            const kills = parseInt(weapon.kills) - parseInt(weaponDecKillsInput.value);
            weaponDecKillsInput.value = kills;
            weaponFormDec.submit();
        })
        .appendTo(weaponFormDec);



    const br = document.createElement('br');
    $(br)
        .appendTo(principalDiv);

});


