<?php

function mostrar($item)
{
    print_r($item);
}


function filter($dataToCompare, $arr, $property)
{
    $found = false;
    for ($i = 0; $i < sizeof($arr); $i++) {
        if ($dataToCompare == $arr[$i][$property]) {
            $found = true;
            return $found;
        }
    }
    return $found;
}


function create_cookie($name, $data, $exta_time)
{
    $date = new DateTime('', new DateTimeZone('America/Buenos_Aires'));
    $time = $date->getTimestamp();
    setcookie($name, $data, $time + $exta_time,'/');
}
