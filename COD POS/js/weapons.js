const weapons = getCookie('weapons');

let jsonData = fromStringToJSON(weapons);


function compare( a, b ) {
    if ( a.id < b.id ){
      return -1;
    }
    if ( a.id > b.id ){
      return 1;
    }
    return 0;
  }
  


const weaponLvUP = 5000;


jsonData = JSON.parse(jsonData);

jsonData.sort( compare );
  



const css_main = html("link", {}, {
    rel: "stylesheet",
    href: "../../css/main.css"
}, document.head);

const css_weapons = html("link", {}, {
    rel: "stylesheet",
    href: "../../css/weapons.css"
}, document.head);




const weaponContainer = html('a', {

}, {
    textContent: 'RETURN',
    href: '../../home.html',
    class: 'text return'
})

jsonData.map(weapon => {

    console.log(weapon);
    const weaponContainer = html('div', {}, { class: 'container' })

    const weaponImage = html('img', {

    }, {
        src: weapon.urlimage,
        class: 'weapon_image'
    }, weaponContainer)



    const weaponTitle = html('h3', {
        margin: '-60vh 0vh 0vh 80vh',
        fontSize: "8vh",
    }, {
        textContent: weapon.name,
        class: 'text',
    }, weaponContainer)

    const weaponKills = html('h3', {
        margin: '10vh 0vh 0vh 80vh',
        fontSize: "6vh"
    }, {
        textContent: "EXP: " + weapon.kills,
        class: 'text',

    }, weaponContainer)

    const weaponPerks = html('h3', {
        margin: '10vh 0vh 0vh 85vh',
        fontSize: "6vh"
    }, {
        textContent: `UNLOCKED PERKS: ${Math.floor(weapon.kills / weaponLvUP)}`,
        class: 'text',
    }, weaponContainer)


    const weaponFormAdd = html('form', {}, {
        method: 'POST',
        class: 'form'
    }, weaponContainer)


    const weaponAddKillsInput = html('input', {
        margin: '0vh 0vh 0vh 120vh',
    }, {
        placeholder: "ADD EXP TO " + weapon.name,
        name: 'kills',
        type: 'number',
        class: 'input'
    }, weaponFormAdd)

    const weaponAddNameWeapon = html('input', {
    }, {
        name: 'name',
        value: weapon.name,
        type: 'hidden'
    }, weaponFormAdd)

    const weaponAddKillsButton = html('input', {
        margin: '2vh 0vh 0vh 5vh',
        padding: "0vh 3vh 0vh 3vh"
    }, {
        value: "+",
        type: 'submit',
        class: 'sub-button'

    }, weaponFormAdd)


    weaponAddKillsButton.addEventListener('click', (e) => {
        e.preventDefault();
        const kills = parseInt(weaponAddKillsInput.value) + parseInt(weapon.kills);
        weaponAddKillsInput.value = kills;
        weaponFormAdd.submit();
    })


    const weaponFormDec = html('form', {}, {
        method: 'POST',
        class: 'form'
    }, weaponContainer)


    const weaponDecKillsInput = html('input', {
        margin: '0vh 0vh 0vh 120vh',
    }, {
        placeholder: "REMOVE EXP TO " + weapon.name,
        name: 'kills',
        type: 'number',
        class: 'input'

    }, weaponFormDec)

    const weaponDecNameWeapon = html('input', {
    }, {
        name: 'name',
        value: weapon.name,
        type: 'hidden'
    }, weaponFormDec)

    const weaponDecKillsButton = html('input', {
        margin: '10vh 0vh 0vh 6vh',
        padding: "0vh 3.5vh 0vh 3.5vh"
    }, {
        value: "-",
        type: 'submit',
        class: 'sub-button'

    }, weaponFormDec)


    weaponDecKillsButton.addEventListener('click', (e) => {
        e.preventDefault();
        const kills = parseInt(weapon.kills) - parseInt(weaponDecKillsInput.value);
        weaponDecKillsInput.value = kills;
        weaponFormDec.submit();
    })

    html('br', {}, {},)
})

