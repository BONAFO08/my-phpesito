<?php


// function arr_push($arr, $data, $index = false)
// {
//     if ($index == false) {
//         return $arr[1] =  $data;
//     } else {
//         return $arr[$index] =  $data;
//     }
// }

function filter_numeral_arr($dataToSearch, $conditional, $arr, $property = false)
{
    $result = (object) [
        'data' => array(),
        'boolean' => false
    ];


    switch ($conditional) {
        case '==':
            for ($i = 0; $i < sizeof($arr); $i++) {
                if ($property != false) {
                    if ($dataToSearch == $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch == $arr[$i]) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;

        case '!=':
            for ($i = 0; $i < sizeof($arr); $i++) {


                if ($property != false) {
                    if ($dataToSearch != $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch != $arr[$i]) {

                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;

        case '===':



            for ($i = 0; $i < sizeof($arr); $i++) {

                if ($property != false) {
                    if ($dataToSearch === $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch === $arr[$i]) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;

        case '!==':


            for ($i = 0; $i < sizeof($arr); $i++) {


                if ($property != false) {
                    if ($dataToSearch !== $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch !== $arr[$i]) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;

        case '>=':

            for ($i = 0; $i < sizeof($arr); $i++) {


                if ($property != false) {
                    if ($dataToSearch >= $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch >= $arr[$i]) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;


        case '<=':

            for ($i = 0; $i < sizeof($arr); $i++) {

                if ($property != false) {
                    if ($dataToSearch <= $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch <= $arr[$i]) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;

        case '>':

            for ($i = 0; $i < sizeof($arr); $i++) {

                if ($property != false) {

                    if ($dataToSearch > $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                } else {

                    if ($dataToSearch > $arr[$i]) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;


        case '<':

            for ($i = 0; $i < sizeof($arr); $i++) {

                if ($property != false) {
                    if ($dataToSearch < $arr[$i]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch < $arr[$i]) {
                        $result->data[sizeof($result->data)] = $arr[$i];

                        $result->boolean = true;
                    }
                }
            }
            break;

        default:
            $result->data = 'Error. You need to send an valid operator';
            break;
    }
    return $result;
}


function filter_asociative_arr($dataToSearch, $conditional, $arr, $property = false)
{
    $result = (object) [
        'data' => array(),
        'boolean' => false
    ];



    switch ($conditional) {

        case '==':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch == $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch == $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        case '!=':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch != $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch != $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        case '===':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch === $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch === $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        case '!==':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch !== $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch !== $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        case '>=':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch >= $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch >= $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        case '<=':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch <= $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch <= $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        case '>':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch > $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch > $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        case '<':
            foreach ($arr as $key => $data) {

                if ($property != false) {

                    if ($dataToSearch < $arr[$key]->$property) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                } else {
                    if ($dataToSearch < $arr[$key]) {
                        $result->data[sizeof($result->data)] = $arr[$key];
                        $result->boolean = true;
                    }
                }
            }
            break;


        default:
            $result->data = 'Error. You need to send an valid operator';
            break;
    }



    return $result;
}


function filter_arr($dataToSearch, $conditional, $arr, $property = false)
{
    if (is_array($arr)) {
        error_reporting(E_ERROR);
        if (!is_null($arr[0])) {
            return  filter_numeral_arr($dataToSearch, $conditional, $arr, $property);
        } else {
            return  filter_asociative_arr($dataToSearch, $conditional, $arr, $property);
        }
    } else {
        return 'Error. You need to send an arr!!';
    }
}
