<?php
require __DIR__ . '/cookie_creator.php';
require __DIR__ . '/mysql/mysql_functions.php';

if ($_POST) {

    $data_to_search = $_POST['data_character_load'];
    $response = show_character_data($data_to_search, 'cha_name', $conexion);
    if ($response->status == 200) {
        create_cookie("playerData", json_encode($response), 90);
        include('../views/home.html');
    } else {
        create_cookie("error", $response->msj, 60);
        include('../views/error.html');
    }

    
    // function prepare_search_character_data($seachParam, $data_to_search, $conexion)

    // {
    //     switch ($seachParam) {
    //         case 'name':
    //             $response = show_character_data($data_to_search, 'cha_name', $conexion);
    //             if ($response->status == 200) {
    //                 show_data($response);
    //             } else {
    //                 mostrar($response['msj']);
    //             }
    //             break;
    //             break;
    //         default:
    //             mostrar('FAIL');
    //             break;
    //     }
    // };

    // function show_data($cha_data)
    // {
    //     $stats = json_decode($cha_data -> cha_stats);

    //     print_r("<br><br><br>");
    //     print_r("PLAYER INFORMATION");
    //     print_r("<br><br>");
    //     print_r("NAME: &nbsp&nbsp&nbsp" . $cha_data -> cha_name);
    //     print_r("<br><br>");
    //     print_r("RACE: &nbsp&nbsp&nbsp" . $cha_data -> cha_race);
    //     print_r("<br><br>");
    //     print_r("STRENGHT: &nbsp&nbsp&nbsp" . $stats->strenght);
    //     print_r("<br><br>");
    //     print_r("AGILITY: &nbsp&nbsp&nbsp" . $stats->agility);
    //     print_r("<br><br>");
    //     print_r("VITALITY: &nbsp&nbsp&nbsp" . $stats->vitality);
    //     print_r("<br><br>");
    //     print_r("ENERGY: &nbsp&nbsp&nbsp" . $stats->energy);
    // }

    // prepare_search_character_data($data_to_search->searchParam, $data_to_search->name, $conexion);
}
