<?php

require __DIR__ . '/mysql/mysql_functions.php';
require __DIR__ . '/cookie_creator.php';
require __DIR__ . '/models/character_model.php';

if ($_POST) {
    $character = new Character();
    $cha_stats = array();
    $cha_stats['strenght'] = $_POST['strenght'];
    $cha_stats['agility'] = $_POST['agility'];
    $cha_stats['vitality'] = $_POST['vitality'];
    $cha_stats['energy'] = $_POST['energy'];
    $character->asign_character_data($_POST['name'],$cha_stats,$_POST['race']);
    create_cookie("playerData", json_encode($character), 90);
    $character->save_player_data($conexion);
    include('../views/home.html');
}