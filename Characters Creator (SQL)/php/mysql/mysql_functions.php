<?php

require __DIR__ . '/mysql_conexion.php';


function sql_generator_insert($tableName, $data_arr)
{
    $part1 = "INSERT INTO `$tableName` (";
    $part2 = "";
    $part3 = ") VALUES (";
    $part4 = "";
    $part5 = ");";



    for ($i = 0; $i < sizeof($data_arr); $i++) {
        if ($i == (sizeof($data_arr) - 1)) {
            $part2 = $part2 . "`" . $data_arr[$i]->key . "`";
            $part4 = $part4 . "'" . $data_arr[$i]->value . "'";
        } else {
            $part2 = $part2 . "`" .  $data_arr[$i]->key . "`" . ',';
            $part4 = $part4 . "'" . $data_arr[$i]->value . "'"  . ',';
        }
    }

    return $part1 . $part2 . $part3 . $part4 . $part5;
}







// function save_character($name, $stats, $race, $sql_conexion)
function save_character($data_arr, $sql_conexion)
{
    try {
        // $serialized_stats = json_encode($stats);
        // $sql = "INSERT INTO `characters` (`id`, `cha_name`, `cha_stats`, `cha_race`) VALUES (NULL, '$name', '$serialized_stats', '$race');";
        $sql = sql_generator_insert('characters', $data_arr);
        $preparation =  $sql_conexion->prepare($sql);
        $response = $preparation->execute();
        return $response;
    } catch (\Throwable $error) {
        mostrar($error);
    }
}

function show_character_data($name, $search_param, $sql_conexion)
{
    try {
        $sql = "SELECT * FROM `characters` WHERE `$search_param` = '$name'";
        $preparation = $sql_conexion->prepare($sql);
        $preparation->execute();
        $character_data = $preparation->fetchAll();
        if (sizeof($character_data) >= 1) {
            $character_data = (object) $character_data[0];
            $character_data->status = 200;
            return $character_data;
        } else {
            return (object) ["status" => 404, "msj" => 'ERROR! THE CHARACTER ' . $name . " DOESN'T EXIST"];
        }
    } catch (\Throwable $error) {
        mostrar($error);
    }
}

function mostrar($item)
{
    print_r($item);
}

function booleanValidator($boolean)
{
    if ($boolean) {
        return "true";
    } else {
        return "false";
    }
}
