<?php

// require "C:/xampp/htdocs/excersices/Characters Creator (SQL)/php/mysql/mysql_functions.php";


class BaseActor
{
    public $cha_name;
}

class Character extends BaseActor
{
    private $cha_stats;
    private $cha_race;

    public function asign_character_data($name,$stats, $race)
    {
        $this->cha_name = $name;
        $this->cha_stats = $stats;
        $this->cha_race = $race;
    }

    public function save_player_data($my_conexion)
    {
        $character_data = (object) [
            'cha_name' => $this->cha_name,
            'cha_stats' => json_encode($this->cha_stats),
            'cha_race' => $this->cha_race,
            'id' => "NULL",

        ];

        $data_arr = array();
  

        foreach ($character_data as $key => $value) {
            $object = (object) ['key' => $key, 'value' => $value];
            array_push($data_arr,$object);
        }


        save_character($data_arr,$my_conexion);

        
    }
}
