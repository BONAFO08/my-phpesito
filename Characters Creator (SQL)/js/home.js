


const cookie = fromStringToJSON(getCookie('playerData'));
const playerData = JSON.parse(cookie);


const optionDiv = document.getElementById('optionsDiv');

const formSearch = document.getElementById('formSearch');



document.getElementById('title').textContent = `${playerData.cha_name}, WELCOME TO WAIFU BATTLE!`;

document.getElementById('searchButton').addEventListener('click', () => {
    formSearch.hidden = false;
    optionDiv.hidden = true;
})

document.getElementById('searchInputButton').addEventListener('click', (e) => {
    e.preventDefault();
    const data = { data: { cha_name: document.getElementById('searchTextInput').value, searchParam: 'cha_name' } };
    document.getElementById('searchHiddenInput').value = JSON.stringify(data);
    formSearch.submit();
})

console.log(playerData);
