const playerName = getCookie('name');
const playerRace = getCookie('race');

const sendData = () => {
    const inputName = document.createElement('input');
    inputName.type = 'hidden';
    inputName.name = 'name';
    inputName.value = playerName;

    const inputRace = document.createElement('input');
    inputRace.type = 'hidden';
    inputRace.name = 'race';
    inputRace.value = playerRace;

    const inputStrenght = document.createElement('input');
    inputStrenght.type = 'hidden';
    inputStrenght.name = 'strenght';
    inputStrenght.value = baseStats.strenght;

    const inputAgility = document.createElement('input');
    inputAgility.type = 'hidden';
    inputAgility.name = 'agility';
    inputAgility.value = baseStats.agility;

    const inputVitality = document.createElement('input');
    inputVitality.type = 'hidden';
    inputVitality.name = 'vitality';
    inputVitality.value = baseStats.vitality;

    const inputEnergy = document.createElement('input');
    inputEnergy.type = 'hidden';
    inputEnergy.name = 'energy';
    inputEnergy.value = baseStats.energy;



    form.appendChild(inputName);
    form.appendChild(inputRace);
    form.appendChild(inputStrenght);
    form.appendChild(inputAgility);
    form.appendChild(inputVitality);
    form.appendChild(inputEnergy);

}


const form = document.getElementById('form');



document.getElementById('sendButton').addEventListener('click', (e) => {
    e.preventDefault();

    sendData();
    form.submit();
})


let pointsToSpend = 15;

const baseStats = {
    strenght: 0,
    agility: 0,
    vitality: 0,
    energy: 0,
};


document.getElementById('title').textContent = `PLEASE SELECT YOUR STATS ${getCookie('name')}`;


document.getElementById("pointsToSpend").innerHTML = `YOU HAVE <span style="color : #ffffff;">${pointsToSpend}</span> POINTS TO SPEND`;




const setBaseStats = () => {




    switch (playerRace) {

        case 'warrior':
            baseStats.strenght = 20;
            baseStats.agility = 5;
            baseStats.vitality = 10;
            baseStats.energy = 0;
            break;


        case 'mage':
            baseStats.strenght = 5;
            baseStats.agility = 10;
            baseStats.vitality = 10;
            baseStats.energy = 20;
            break;


        case 'tank':
            baseStats.strenght = 10;
            baseStats.agility = 5;
            baseStats.vitality = 20;
            baseStats.energy = 5;
            break;


        case 'archer':
            baseStats.strenght = 5;
            baseStats.agility = 20;
            baseStats.vitality = 10;
            baseStats.energy = 5;
            break;

    }



    document.getElementById('strenghtLabel').textContent = baseStats.strenght;
    document.getElementById('agilityLabel').textContent = baseStats.agility;
    document.getElementById('vitalityLabel').textContent = baseStats.vitality;
    document.getElementById('energyLabel').textContent = baseStats.energy;

}

const addStat = (stat) => {
    pointsToSpend -= 1;
    baseStats[stat] += 1;
    document.getElementById("pointsToSpend").innerHTML = `YOU HAVE <span style="color : #ffffff;">${pointsToSpend}</span> POINTS TO SPEND`;
    document.getElementById('strenghtLabel').textContent = baseStats.strenght;
    document.getElementById('agilityLabel').textContent = baseStats.agility;
    document.getElementById('vitalityLabel').textContent = baseStats.vitality;
    document.getElementById('energyLabel').textContent = baseStats.energy;

}

const redStat = (stat) => {
    pointsToSpend += 1;
    baseStats[stat] -= 1;
    document.getElementById("pointsToSpend").innerHTML = `YOU HAVE <span style="color : #ffffff;">${pointsToSpend}</span> POINTS TO SPEND`;
    document.getElementById('strenghtLabel').textContent = baseStats.strenght;
    document.getElementById('agilityLabel').textContent = baseStats.agility;
    document.getElementById('vitalityLabel').textContent = baseStats.vitality;
    document.getElementById('energyLabel').textContent = baseStats.energy;

}

const hiddeAddButtons = () => {
    document.getElementById('strenghtButtonAdd').style.display = 'none';
    document.getElementById('agilityButtonAdd').style.display = 'none';
    document.getElementById('vitalityButtonAdd').style.display = 'none';
    document.getElementById('energyButtonAdd').style.display = 'none';
}

const showAddButtons = () => {
    document.getElementById('strenghtButtonAdd').style.display = 'inline-block';
    document.getElementById('agilityButtonAdd').style.display = 'inline-block';
    document.getElementById('vitalityButtonAdd').style.display = 'inline-block';
    document.getElementById('energyButtonAdd').style.display = 'inline-block';
}




document.getElementById('strenghtButtonAdd').addEventListener('click', () => {
    if (pointsToSpend == 1) {
        hiddeAddButtons();
        addStat('strenght');
    } else {
        addStat('strenght');
        document.getElementById('strenghtButtonRed').style.display = 'inline-block';
    }
});





document.getElementById('strenghtButtonRed').addEventListener('click', () => {
    if (pointsToSpend >= 0) {
        showAddButtons();
        if (baseStats.strenght == 1) {
            document.getElementById('strenghtButtonRed').style.display = 'none';
            redStat('strenght');
        } else {
            redStat('strenght');
        }
    }
});

document.getElementById('agilityButtonAdd').addEventListener('click', () => {
    if (pointsToSpend == 1) {
        hiddeAddButtons();
        addStat('agility');
    } else {
        addStat('agility');
        document.getElementById('agilityButtonRed').style.display = 'inline-block';
    }
});

document.getElementById('agilityButtonRed').addEventListener('click', () => {
    if (pointsToSpend >= 0) {
        showAddButtons();
        if (baseStats.agility == 1) {
            document.getElementById('agilityButtonRed').style.display = 'none';
            redStat('agility');
        } else {
            redStat('agility');
        }
    }
});

document.getElementById('vitalityButtonAdd').addEventListener('click', () => {
    if (pointsToSpend == 1) {
        hiddeAddButtons();
        addStat('vitality');
    } else {
        addStat('vitality');
        document.getElementById('vitalityButtonRed').style.display = 'inline-block';
    }
});

document.getElementById('vitalityButtonRed').addEventListener('click', () => {
    if (pointsToSpend >= 0) {
        showAddButtons();
        if (baseStats.vitality == 1) {
            document.getElementById('vitalityButtonRed').style.display = 'none';
            redStat('vitality');
        } else {
            redStat('vitality');
        }
    }

});

document.getElementById('energyButtonAdd').addEventListener('click', () => {
    if (pointsToSpend == 1) {
        hiddeAddButtons();
        addStat('energy');
    } else {
        addStat('energy');
        document.getElementById('energyButtonRed').style.display = 'inline-block';
    }

});

document.getElementById('energyButtonRed').addEventListener('click', () => {

    if (pointsToSpend >= 0) {
        showAddButtons();
        if (baseStats.energy == 1) {
            document.getElementById('energyButtonRed').style.display = 'none';
            redStat('energy');
        } else {
            redStat('energy');
        }
    }



});

setBaseStats();


