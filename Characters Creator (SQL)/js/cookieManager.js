
const getCookie = (name) => {
    const value = `; ${document.cookie}`;
    const parts = value.split(`; ${name}=`);
    if (parts.length === 2) return decodeURI(parts.pop().split(';').shift());
}

const fromStringToJSON = (string) => {
    let counter = 0;

    do {
        string = string.replace('%3A', ':');
        string = string.replace('%2C', ',');
        if (!string.includes('%3A') && !string.includes('%2C')) {
            counter = 200;
        }
    } while (counter < 100);
    return string;
}